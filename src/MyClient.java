import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.server.ExportException;

/*
Created by Raul Vasquez 10/12/2015
*/
public class MyClient implements Runnable {
    InetAddress server;
    int port;
    String message;
    Socket client_socket;

    public MyClient(String argc, String argv,String message) {
        this.server = HostResolve.getHost(argc);
        this.port = Integer.valueOf(argv);
        this.message =  message;
    }

    public void run(){
        connect();
        transferfile();

    }
    public void connect(){
        try {
            client_socket = new Socket(server, port);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Client Connected! getting: " + message);
    }
    public void transferfile() {
        try{
            /*
            This methods will get streams to get message from STDIN (a file name)
            then will send that message. If the request if valid the server will
            return a long(the size of the binary file transfered)
            */
            DataOutputStream output = new DataOutputStream(client_socket.getOutputStream());
            DataInputStream input = new DataInputStream(client_socket.getInputStream());
            output.writeUTF(message);

            //Wait for long, if 0 stop
            long byteSize = input.readLong();
            int bytesize = ((int) byteSize);
            if (byteSize == 0){
                System.out.println("File not found");
                return;
            }
            byte[] messageByte = new byte[bytesize];
            try (InputStream raw = client_socket.getInputStream()) {
                InputStream in = new BufferedInputStream(raw);
                byte[] data = new byte[bytesize];
                int offset = 0;
                while (offset < bytesize) {
                    int bytesRead = in.read(data, offset, data.length - offset);
                    if (bytesRead == -1) break;
                    offset += bytesRead;
                }
                if (offset != bytesize) {
                    throw new IOException("Only read " + offset
                            + " bytes; Expected " + bytesize + " bytes");
                }
                String filename = message;
                filename = filename.substring(filename.lastIndexOf('/') + 1);
                try (FileOutputStream fout = new FileOutputStream(filename)) {
                    fout.write(data);
                    fout.flush();
                    fout.close();
                }
            }
            System.out.println("Received: " + message);
            client_socket.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
