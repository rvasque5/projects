import java.net.InetAddress;

/**
 * Created by mint on 10/15/15.
 */
public class HostResolve {
    public static InetAddress getHost(String argin) {
        try {
            InetAddress host = InetAddress.getByName(argin);
            return host;
        } catch (Exception e) {
            System.out.println("Could not resolve host");
        }
        return null;
    }
    public static InetAddress[] getAllHost (String argin){
        try {
            InetAddress[] netAddresses = InetAddress.getAllByName(argin);
            return netAddresses;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public static InetAddress getLoopBack(){
        return InetAddress.getLoopbackAddress();
    }
}
