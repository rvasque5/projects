import java.io.BufferedReader;
import java.io.InputStreamReader;

/*
Created by Raul Vasquez 10/12/2015
*/
public class MultiBinaryClient {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: java MyClient <server> <port> ");
            return;
        }
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));
        String[] messages = new String[6];
        //Gets each line
        for(int i = 0; i < 6; i++) {
            try {
                messages[i] = reader.readLine();
            }
            catch (Exception e){
            }
        }
        for(String s: messages){
            if(s !=  null) {
                MyClient mbc = new MyClient(args[0], args[1], s);
                Thread t1 = new Thread(mbc);
                t1.start();
            }
        }
    }
}
