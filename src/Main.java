import java.net.InetAddress;

/**
 * Created by mint on 10/15/15.
 */
//This class will be used to test individual classes.
public class Main {
    public static  void main (String[] args){
        InetAddress lb = HostResolve.getLoopBack();
        ServerSocket myServer =  new ServerSocket(5000);
        myServer.run();

    }
}
